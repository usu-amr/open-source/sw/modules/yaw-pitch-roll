# node reads imu data in x, y, z, and w and converts it to yaw/pitch/roll
# node publishes yaw pitch and roll float32 values
# node subscribes to imu

#!/usr/bin/env python
import rospy
from std_msgs.msg import Float32
from sensor_msgs.msg import Imu
from numpy import linalg as LA
import numpy as np
from tf.transformations import euler_from_quaternion
import math

#sets publishing to none for now
yaw_pub = None
pitch_pub = None
roll_pub = None

#gets imu_data
def convert_imu_data(data):
    quaternion = (
        data.orientation.x,
        data.orientation.y,
        data.orientation.z,
        data.orientation.w
    )
    #converts to yaw pitch and roll
    euler = euler_from_quaternion(quaternion)
    roll = -euler[0] * (180 / math.pi) # the - should be here
    pitch = euler[1] * (180 / math.pi)
    yaw = euler[2] * (180 / math.pi)
    #publishes values
    yaw_pub.publish(Float32(yaw))
    pitch_pub.publish(Float32(pitch))
    roll_pub.publish(Float32(roll))

def node():
    global yaw_pub, pitch_pub, roll_pub
    rospy.init_node('yaw_pitch_roll')
    
    rospy.Subscriber("/imu", Imu, convert_imu_data, queue_size=1)
    
    yaw_pub = rospy.Publisher('/yaw', Float32, queue_size=1)
    pitch_pub = rospy.Publisher('/pitch', Float32, queue_size=1)
    roll_pub = rospy.Publisher('/roll', Float32, queue_size=1)
    
    rate = rospy.Rate(100) # 100hz
    while not rospy.is_shutdown():
        rate.sleep()

if __name__ == '__main__':
    try:
        node()
    except rospy.ROSInterruptException:
        pass